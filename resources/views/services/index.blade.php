@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Services</h2>
            </div>
            <div class="pull-right">
                @can('service-create')
                    <a class="btn btn-success" href="{{ route('services.create') }}"> Create New Service</a>
                @endcan
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Details</th>
            <th width="280px">Action</th>
        </tr>
	    @foreach ($services as $service)
	    <tr>
	        <td>{{ ++$i }}</td>
	        <td>{{ $service->name }}</td>
	        <td>{{ $service->detail }}</td>
	        <td>
                <form action="{{ route('services.destroy',$service->id) }}" method="POST">
                    <a class="btn btn-info" href="{{ route('services.show',$service->id) }}">Show</a>
                    @can('service-edit')
                        <a class="btn btn-primary" href="{{ route('services.edit',$service->id) }}">Edit</a>
                    @endcan
                    @csrf
                    @method('DELETE')
                    @can('service-delete')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    @endcan
                </form>
                <form action="{{ route('services.purchase', ['id' => $service->id]) }}" method="POST">
                    @csrf <br>
                    <script src="https://checkout.razorpay.com/v1/checkout.js"
                    data-key="rzp_test_ytr62VGcJ69Tlr"
                    data-amount="10000"
                    data-buttontext="Purchase"
                    data-name="TartLabs"
                    data-description="Razorpay"
                    data-image="https://cdn.tartlabs.com/wp-content/uploads/2019/02/B4FEFF78-93DB-4C4B-A8E1-6C98E4F8FDA9.png"
                    data-prefill.name=""
                    data-prefill.email=""
                    data-theme.color="#ff7529">
                    </script>
                </form>
                </form>
	        </td>
	    </tr>
	    @endforeach
    </table>
    {!! $services->links() !!}
@endsection