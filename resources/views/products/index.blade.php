@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Products</h2>
            </div>
            <div class="pull-right">
                @can('product-create')
                    <a class="btn btn-success" href="{{ route('products.create') }}"> Create New Product</a>
                @endcan
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Details</th>
            <th width="280px">Action</th>
        </tr>
	    @foreach ($products as $product)
	    <tr>
	        <td>{{ ++$i }}</td>
	        <td>{{ $product->name }}</td>
	        <td>{{ $product->detail }}</td>
	        <td>
                <form action="{{ route('products.destroy',$product->id) }}" method="POST">
                    <a class="btn btn-info" href="{{ route('products.show', $product->id) }}">Show</a>
                    @can('product-edit')
                        <a class="btn btn-primary" href="{{ route('products.edit', $product->id) }}">Edit</a>
                    @endcan
                    @csrf
                    @method('DELETE')
                    @can('product-delete')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    @endcan
                </form>
                <form action="{{ route('products.purchase', ['id' => $product->id]) }}" method="POST">
                    @csrf <br>
                    <script src="https://checkout.razorpay.com/v1/checkout.js"
                    data-key="rzp_test_ytr62VGcJ69Tlr"
                    data-amount="10000"
                    data-buttontext="Purchase"
                    data-name="TartLabs"
                    data-description="Razorpay"
                    data-image="https://cdn.tartlabs.com/wp-content/uploads/2019/02/B4FEFF78-93DB-4C4B-A8E1-6C98E4F8FDA9.png"
                    data-prefill.name=""
                    data-prefill.email=""
                    data-theme.color="#ff7529">
                    </script>
                </form>
	        </td>
	    </tr>
	    @endforeach
    </table>
    {!! $products->links() !!}
@endsection