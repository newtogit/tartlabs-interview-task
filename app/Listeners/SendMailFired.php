<?php

namespace App\Listeners;

use App\Events\SendMail;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use niklasravnsborg\LaravelPdf\Facades\Pdf;

class SendMailFired
{
    public function __construct()
    {
        //
    }
    
    public function handle(SendMail $event)
    {

        $user = User::getUser($event->userId)->toArray();
        $purchaseOrder = $event->purchase;

        $data = [
            "user_name" => $user['name'],
            "email"     => $user['email'],
            "title"     => "Mail for Purchase Confirmation.",
            "name"      => $purchaseOrder->name,
            "amount"    => "100",
        ];
  
        $pdf = PDF::loadView('emails.purchaseMailPdf', $data);
  
        Mail::send('emails.mailEvent', $data, function($message)use($data, $pdf) {
            $message->to($data["email"])
                    ->subject($data["title"])
                    ->attachData($pdf->output(), "invoice.pdf");
        });
    }
}
