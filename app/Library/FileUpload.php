<?php
namespace App\Library;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;
class FileUpload
{
    private static $validImageExtensions = [
        'jpg',
        'jpeg',
        'png',
        'gif',
        'svg',
    ];
    public static function store($path, $file, $oldFileName = null)
    {
        if (!is_null($oldFileName)) {
            //Unlink
            FileUpload::destory($path, $oldFileName);
        }
        $imageExtension = $file->getClientOriginalExtension();
        $fileName = Str::uuid($file).'.'.$imageExtension;
        Storage::putFileAs('/public/uploads/'.$path.'/', $file, $fileName);
        //Thumbnail
        if (in_array($imageExtension, FileUpload::$validImageExtensions)) {
            $img = Image::make($file->path());
            $img->orientate();
            $img->resize(100, 100, function ($constraint){
                $constraint->aspectRatio();
            })->encode($imageExtension);
            Storage::put('/public/uploads/'.$path.'/'.'thumbnail/'.$fileName, $img);
        }
        return $fileName;
    }
    
    public static function destory($path, $oldFileName)
    {
        Storage::delete('/public/uploads/'.$path.'/'.$oldFileName);
        $imageExtension = explode(".", $oldFileName);
        //Validate Thumbnail
        if (in_array($imageExtension[1], FileUpload::$validImageExtensions)) {
            Storage::delete('/public/uploads/'.$path.'/'.'thumbnail/'.$oldFileName);
        }
        return TRUE;
    }
}