<?php

namespace App\Library;

use Illuminate\Support\Facades\Http;
use GuzzleHttp\Client;

class HttpRequest
{
    
    public static function httpGet($url, $headers)
    {
        $getProfileValues = Http::withHeaders($headers)->get($url);
        return json_decode($getProfileValues->getBody(), true);
    }

    public static function httpPost($url, $headers, $body)
    {
        $getAccessToken = Http::asForm()->withHeaders($headers)->post($url, $body);
        return json_decode($getAccessToken->getBody(), true);
    }
    
    public static function clientPost($url, $headers, $body)
    {
        $client = new client();
        $getWebinarDetails  = $client->request('POST', $url, [
            'headers' => $headers,
            'body' => $body,
        ]);
        return json_decode($getWebinarDetails->getBody(), true);
    }
}