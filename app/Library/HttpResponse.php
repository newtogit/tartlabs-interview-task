<?php
namespace App\Library;
use Exception;
use Illuminate\Http\JsonResponse;
class HttpResponse
{
    /**
     * List of valid HTTP error codes
     * Refer: https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
     *
     * @var array
     */
    private static $validHttpErrorCodes = [
        400,
        401,
        402,
        403,
        404,
        405,
        406,
        407,
        408,
        409,
        410,
        411,
        412,
        413,
        414,
        415,
        416,
        417,
        418,
        421,
        422,
        423,
        424,
        425,
        426,
        428,
        429,
        431,
        451,
        500,
        501,
        502,
        503,
        504,
        505,
        506,
        507,
        508,
        510,
        511,
    ];
    /**
     * Returns a error response
     *
     * @param $payload
     * @param $code
     * @return \Illuminate\Http\Response | \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public static function error($payload, $code = 400)
    {
        if (!isset($code) || !in_array($code, HttpResponse::$validHttpErrorCodes)) {
            $code = 500;
        }
        $responsePayload = [
            'status' => false,
        ];
        if (is_array($payload)) {
            $responsePayload = array_merge($responsePayload, $payload);
        } elseif (is_string($payload)) {
            $responsePayload['error'] = $payload;
        } else {
            throw new Exception('Response payload is not valid', 500);
        }
        return response()->json($responsePayload, $code);
    }
    /**
     * Return a success response
     *
     * @param $payload
     * @return JsonResponse
     * @throws Exception
     */
    public static function success($payload = null)
    {
        $responsePayload = [
            'status' => true,
        ];
        if (!is_null($payload)) {
            if (is_array($payload)) {
                $responsePayload = array_merge($responsePayload, $payload);
            } elseif (is_string($payload)) {
                $responsePayload['message'] = $payload;
            } else {
                throw new Exception('Response payload is not valid', 500);
            }
        }
        return response()->json($responsePayload, 200);
    }
}