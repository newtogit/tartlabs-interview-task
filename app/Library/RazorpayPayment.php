<?php
namespace App\Library;
use Razorpay\Api\Api;
use Exception;
class RazorpayPayment
{
    
    public static function store($input)
    {
        $api = new Api(env('RAZORPAY_KEY'), env('RAZORPAY_SECRET'));
        $payment = $api->payment->fetch($input['razorpay_payment_id']);
        if (count($input)  && !empty($input['razorpay_payment_id'])) {
            try {
                $response = $api->payment->fetch($input['razorpay_payment_id'])->capture(array('amount'=>$payment['amount'])); 
            } catch (Exception $e) {
                return  $e->getMessage();
                return redirect()->back()->with('success', $e->getMessage());
            }
        }
        return array(
            'razorpay_payment_id' => $input['razorpay_payment_id'],
            'amount' => $payment['amount'],
        );
    }
}