<?php

namespace App\Exceptions;

use \Illuminate\Contracts\Validation\Validator;

class InvalidRequestException extends AppException
{
    public $errorArray;
    public function __construct(Validator $validator)
    {
        parent::__construct();
        $this->message = implode(', ', $validator->errors()->all());
        $this->code = 400;
        $this->errorArray = $validator->errors();
    }
}