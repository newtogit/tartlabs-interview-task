<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SendMail
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    
    public $userId, $purchase;
    public function __construct($userId, $purchase)
    {
        $this->userId = $userId;
        $this->purchase = $purchase;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
