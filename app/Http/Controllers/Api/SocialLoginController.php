<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\Library\HttpResponse;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Exception;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers\Controller as Controller;

class SocialLoginController extends Controller
{
    use AuthenticatesUsers;

    public function handleProviderCallback(Request $request)
    {
        $token = $request->input('token');
        $provider = $request->input('provider');
        try {
            $user = Socialite::driver($provider)->userFromToken($token);
            $finduser = User::verifyUser($user->email);
            if ($finduser) {
                SocialLoginController::authLogin($finduser);
                return HttpResponse::success(["data" => 'You are loggedin successfully using '.$provider]);
            } else {
                $newUser = [
                    'name' => $user->name,
                    'email' => $user->email,
                    'provider_name' => $provider,
                    'provider_id' => $user->id,
                    'password' => Hash::make('12345')
                ];
                $user = User::storeUser($newUser);
                SocialLoginController::authLogin($user);
                return HttpResponse::success(["data" => 'You are loggedin successfully using '.$provider]);
            }
        } catch (Exception $exception) {
            return HttpResponse::error($exception->getMessage(), 400);
        }
    }

    public static function authLogin($finduser)
    {
        Auth::login($finduser);
    }

    public function fbLogin(Request $request)
    {
        $fields = $request->input('fields');
        $token = $request->input('token');
        $response = Http::get('https://graph.facebook.com/v11.0/me', [
            'fields' => $fields,
            'access_token' => $token,
        ]);
        $result = json_decode($response->getBody(), true);
        if (!isset($result['error'])) {
            $finduser = User::verifyUser($result['email']);
            if ($finduser) {
                SocialLoginController::authLogin($finduser);
                return HttpResponse::success(["data" => 'You are loggedin successfully using facebook']);
            } else {
                $newUser = [
                    'name' => $result['name'],
                    'email' => $result['email'],
                    'provider_name' => 'facebook',
                    'provider_id' => $result['id'],
                    'password' => Hash::make('12345')
                ];
                $user = User::storeUser($newUser);
                SocialLoginController::authLogin($user);
                return HttpResponse::success(["data" => 'You are loggedin successfully using facebook']);
            }
        } else {
            return HttpResponse::error($result['error'], 400);
        }
    }

    public function linkedinLogin(Request $request)
    {
        $code = $request->input('code');
        $getAccessToken = Http::get('https://www.linkedin.com/oauth/v2/accessToken?', [
            'grant_type'    => 'authorization_code',
            'code'          => $code,
            'client_id'     => env('LINKEDIN_CLIENT_ID'),
            'client_secret' => env('LINKEDIN_CLIENT_SECRET'),
            'redirect_uri'      => env('LINKEDIN_CALLBACK_URL'),
        ]);
        $resultOfAccessToken = json_decode($getAccessToken->getBody(), true);
        if (!isset($resultOfAccessToken['error'])) {
            $getUserDetail = Http::get('https://api.linkedin.com/v2/me?', [
                'oauth2_access_token'   => $resultOfAccessToken['access_token'],
            ]);
            $resultOfUserDetail = json_decode($getUserDetail->getBody(), true);
            if (!isset($resultOfUserDetail['error'])) {
                $getUserEmail = Http::get('https://api.linkedin.com/v2/emailAddress?', [
                    'q'                     => 'members',
                    'projection'            => '(elements*(handle~))',
                    'oauth2_access_token'   => $resultOfAccessToken['access_token'],
                ]);
                $resultOfUserEmail = json_decode($getUserEmail->getBody(), true);
                if (!isset($resultOfUserEmail['error'])) {
                    $email = $resultOfUserEmail['elements'][0]['handle~']['emailAddress'];
                    $finduser = User::verifyUser($email);
                    if ($finduser) {
                        SocialLoginController::authLogin($finduser);
                        return HttpResponse::success(["data" => 'You are loggedin successfully using linkedin']);
                    } else {
                        $newUser = [
                            'name' => $resultOfUserDetail['localizedFirstName'],
                            'email' => $email,
                            'provider_name' => 'linkedin',
                            'provider_id' => $resultOfUserDetail['id'],
                            'password' => Hash::make('12345')
                        ];
                        $user = User::storeUser($newUser);
                        SocialLoginController::authLogin($user);
                        return HttpResponse::success(["data" => 'You are loggedin successfully using linkedin']);
                    }
                } else {
                    return HttpResponse::error($resultOfUserEmail['error'], 400);
                }
            } else {
                return HttpResponse::error($resultOfUserDetail['error'], 400);
            }
        } else {
            return HttpResponse::error($resultOfAccessToken['error'], 400);
        }
    }
}