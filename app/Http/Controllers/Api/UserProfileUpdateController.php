<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Library\HttpResponse;
use App\Models\User;
use App\Models\State;
use App\Models\City;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller as Controller;

class UserProfileUpdateController extends Controller
{

    public function userProfileUpdate(Request $request)
    {
        //here 101 is - Static India Code.
        $validator = Validator::make($request->all(), [
            'country_id' => 'required|exists:countries,id',
            'state_id' => ['nullable', 'exclude_unless:country_id,101', 'required_without:state_name', 'exists:states,id'],
            'state_name' => ['nullable', 'exclude_unless:country_id,101', 'required_without:state_id'],
            'city_id'  => ['nullable', 'exclude_unless:country_id,101', 'required_without:city_name', 'exists:cities,id'],
            'city_name'  => ['nullable', 'exclude_unless:country_id,101', 'required_without:city_id'],
        ]);
        
        if ($validator->fails()) {
            return HttpResponse::error(["data" => $validator->errors()]);
        }

        $id = $request->input('id');
        $country_id = $request->input('country_id');
        $state_id = $request->input('state_id');
        $city_id = $request->input('city_id');

        if ((is_null($state_id)) && ($country_id == 101)) {
            $state_name = $request->input('state_name');
            $state_id = State::addState($country_id, $state_name);
        }

        if ((is_null($city_id)) && ($country_id == 101)) {
            $city_name = $request->input('city_name');
            $city_id = City::addCity($state_id, $city_name);
        }

        $user = [
            'country_id' => $country_id,
            'state_id' => $state_id,
            'city_id' => $city_id,
        ];

        $user = User::profileUpdate($id, $user);
        return HttpResponse::success(["data" => 'Your profile updated successfully']);
    }

    public function getUserReports(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => ['required', Rule::in(['day', 'month', 'year'])],
            'order_by' => 'nullable|in:asc,desc',
        ]);

        if ($validator->fails()) {
            return HttpResponse::error(["data" => $validator->errors()]);
        }

        $type = $request->input('type');
        $orderBy = $request->input('order_by');

        if (is_null($orderBy)) {
            $orderBy = 'asc';
        }

        $users = User::select('id', 'created_at', 'updated_at')
        ->orderBy('id', $orderBy)
        ->get()
        ->groupBy(function($date) use($type) {
            if ($type == 'year') {
                return Carbon::parse($date->created_at)->format('Y');
            } else if ($type == 'month') {
                return Carbon::parse($date->created_at)->format('m-Y');
            } else if ($type == 'day') {
                return Carbon::parse($date->created_at)->format('d-m-Y');
            } else{
            }
        })->toArray();
        
        $user = [];
        
        for ($i=0; $i<count($users); $i++) {
            array_push($user , [$type => array_keys($users)[$i], 'data' => array_values($users)[$i]]);
        }
        
        if ($users) {
            return HttpResponse::success(['user' => $user]);
        } else {
            return HttpResponse::error('No Data', 400);
        }
    }

}