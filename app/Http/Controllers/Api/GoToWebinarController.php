<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Library\HttpResponse;
use App\Library\HttpRequest;
use App\Models\GoToWebinar;
use App\Models\AdminSchedule;
use App\Models\Appointment;
use App\Models\AppointmentSchedulerXref;
use App\Http\Controllers\Controller as Controller;
use DateTime;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Exceptions\InvalidRequestException;

class GoToWebinarController extends Controller
{

    public function goToWebinar(Request $request)
    {
        $code = $request->input('code');
        $url = env('GOTOWEBINAR_GETTOKEN');
        $headers = [
            'Content-Type'  => 'application/x-www-form-urlencoded',
            'Authorization' => 'Basic '.env('GOTOWEBINAR_BASE64_ENCODE'),
        ];
        $body = [
            'grant_type'    => 'authorization_code',
            'code'          => $code,
            'client_id'     => env('GOTOWEBINAR_CLIENT_ID'),
            'client_secret' => env('GOTOWEBINAR_CLIENT_SECRET'),
            'redirect_uri'  => env('GOTOWEBINAR_CALLBACK_URL'),
        ];
        $getAccessToken = HttpRequest::httpPost($url, $headers, $body);
        if (!isset($getAccessToken['error'])) {
            $accessToken    = $getAccessToken['access_token'];
            $refreshToken   = $getAccessToken['refresh_token'];
            $expiresIn      = $getAccessToken['expires_in'];
            $accountKey     = $getAccessToken['account_key'];
            $organizerKey   = $getAccessToken['organizer_key'];
            $dateTime       = date('Y-m-d H:i:s');
            $data = array(
                array('key' => 'CODE',          'value' => $code,           'created_at' => $dateTime, 'updated_at' => $dateTime),
                array('key' => 'ACCESS_TOKEN',  'value' => $accessToken,    'created_at' => $dateTime, 'updated_at' => $dateTime),
                array('key' => 'REFRESH_TOKEN', 'value' => $refreshToken,   'created_at' => $dateTime, 'updated_at' => $dateTime),
                array('key' => 'EXPIRES_IN',    'value' => $expiresIn,      'created_at' => $dateTime, 'updated_at' => $dateTime),
                array('key' => 'ACCOUNT_KEY',   'value' => $accountKey,     'created_at' => $dateTime, 'updated_at' => $dateTime),
                array('key' => 'ORGANIZER_KEY', 'value' => $organizerKey,   'created_at' => $dateTime, 'updated_at' => $dateTime),
            );
            GoToWebinar::storeAccessTokenOrganizationKey($data);
            return HttpResponse::success(["data" => 'Access Token and Organization Key Generated Successfully']);
        } else {
            return HttpResponse::error($getAccessToken['error'], 400);
        }
    }

    public function goToWebinarProfile()
    {
        $accessToken = GoToWebinarController::AccessTokenValidator();
        if (is_array($accessToken)) {
            return HttpResponse::error($accessToken, 400);
        } elseif (is_string($accessToken)) {
            $url = env('GOTOWEBINAR_GETADMIN');
            $headers = [
                'Authorization' => 'Bearer '.$accessToken,
            ];
            $getProfileValues = HttpRequest::httpGet($url, $headers);
            if (!isset($getProfileValues['error'])) {
                return HttpResponse::success(["data" => $getProfileValues]);
            } else {
                return HttpResponse::error($getProfileValues['error'], 400);
            }
        } else {
            return HttpResponse::error('No Data', 400);
        }
    }

    public static function AccessTokenValidator()
    {
        $fetchAccessToken = GoToWebinar::fetchGoToWebinarAccessDetails('ACCESS_TOKEN');
        $accessToken = $fetchAccessToken['value'];
        $accessTokenUpdatedAt = $fetchAccessToken['updated_at'];

        $fetchExpiresIn = GoToWebinar::fetchGoToWebinarAccessDetails('EXPIRES_IN');
        $accessTokenExpiresIn = $fetchExpiresIn['value'];
        
        $dateTime = new DateTime($accessTokenUpdatedAt);
        $dateTime->modify("+$accessTokenExpiresIn sec");
        $checkExpired = $dateTime->format('Y-m-d H:i:s');

        $currentDateTime = date("Y-m-d H:i:s");

        if($currentDateTime > $checkExpired) {
            $fetchRefreshToken = GoToWebinar::fetchGoToWebinarAccessDetails('REFRESH_TOKEN');

            $url = env('GOTOWEBINAR_GETTOKEN');
            $headers = [
                'Content-Type'  => 'application/x-www-form-urlencoded',
                'Authorization' => 'Basic '.env('GOTOWEBINAR_BASE64_ENCODE'),
            ];
            $body = [
                'grant_type'    => 'refresh_token',
                'refresh_token' => $fetchRefreshToken['value'],
                'client_id'     => env('GOTOWEBINAR_CLIENT_ID'),
                'client_secret' => env('GOTOWEBINAR_CLIENT_SECRET'),
            ];
            $getNewAccessToken = HttpRequest::httpPost($url, $headers, $body);
            if (!isset($getNewAccessToken['error'])) {
                $accessToken    = $getNewAccessToken['access_token'];
                $key            = 'ACCESS_TOKEN';
                $dateTime       = date('Y-m-d H:i:s');
                $data = [
                    'value' => $accessToken,
                    'updated_at' => $dateTime
                ];
                GoToWebinar::accessTokenUpdate($key, $data);
                return $accessToken;
            } else {
                return $getNewAccessToken['error'];
            }
        }
        return $accessToken;
    }

    public function goToWebinarCreateWebinar()
    {
        $accessToken = GoToWebinarController::AccessTokenValidator();
        $fetchorganizerKey = GoToWebinar::fetchGoToWebinarAccessDetails('ORGANIZER_KEY');
        $organizerKey = $fetchorganizerKey['value'];
        if (is_array($accessToken)) {
            return HttpResponse::error($accessToken, 400);
        } elseif (is_string($accessToken)) {
            $body = '{
                "subject": "xyz",
                "description": "xyz",
                "times": [
                  {
                    "startTime": "2021-08-22T11:15:22Z",
                    "endTime": "2021-08-22T12:15:22Z"
                  }
                ],
                "timeZone": "UTC",
                "type": "single_session",
                "isPasswordProtected": false,
                "recordingAssetKey": "yes",
                "isOndemand": false,
                "experienceType": "CLASSIC",
                "emailSettings": {
                  "confirmationEmail": {
                    "enabled": true
                  },
                  "reminderEmail": {
                    "enabled": true
                  },
                  "absenteeFollowUpEmail": {
                    "enabled": true
                  },
                  "attendeeFollowUpEmail": {
                    "enabled": true,
                    "includeCertificate": true
                  }
                }
            }';
            $headers = [
                'Authorization' => 'Bearer '.$accessToken,
                'Accept'        => 'application/json',
                'Content-Type'  => 'text/plain',
            ];
            $url = env('GOTOWEBINAR_BASEURL').$organizerKey.'/webinars';
            $getWebinarKey = HttpRequest::clientPost($url, $headers, $body);
            if (!isset($getWebinarKey['error'])) {
                $webinarKey = $getWebinarKey['webinarKey'];
                GoToWebinar::storeWebinarKey($webinarKey);
                $body = '[{
                    "external": true,
                    "organizerKey": "'.$organizerKey.'",
                    "givenName": "abc",
                    "email": "abc@fhg.com"
                }]';
                $url = env('GOTOWEBINAR_BASEURL').$organizerKey.'/webinars'.'/'.$webinarKey.'/coorganizers';
                $getCoOrganizer = HttpRequest::clientPost($url, $headers, $body);
                if (!isset($getCoOrganizer['error'])) {
                    for ($i=0; $i<2; $i++) {
                        $randomNameEmail = substr(str_shuffle('abcdefghijklmnopqrstuvwxyz'),1,5);
                        $body = '{
                            "firstName": "'.$randomNameEmail.'",
                            "lastName": "a",
                            "email": "'.$randomNameEmail.'@qwre.com",
                        }';
                        $url = env('GOTOWEBINAR_BASEURL').$organizerKey.'/webinars'.'/'.$webinarKey.'/registrants';
                        $getRegistrants = HttpRequest::clientPost($url, $headers, $body);
                    }
                    if (!isset($getRegistrants['error'])) {
                        return HttpResponse::success(["data" => 'Webinar has Created, Co-Orgaizers and Registrants Added Successfully']);
                    } else {
                        return HttpResponse::error($getRegistrants['error'], 400);
                    }
                } else {
                    return HttpResponse::error($getCoOrganizer['error'], 400);
                }
            } else {
                return HttpResponse::error($getWebinarKey['error'], 400);
            }
        } else {
            return HttpResponse::error('No Data', 400);
        }
    }

    public function goToWebinarCreateScheduleByAdmin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'admin_id'  => 'required|exists:users,id',
            'schedule'  => 'required|array',
            'schedule.*.start_time' => 'required|date_format:Y-m-d H:i:s|before:schedule.*.end_time|after_or_equal:'.date('Y-m-d H:i:s'),
            'schedule.*.end_time'   => 'required|date_format:Y-m-d H:i:s|after:schedule.*.start_time',
        ]);
        
        if ($validator->fails()) {
            throw new InvalidRequestException($validator);
        }
        
        $adminId = $request->input('admin_id');
        $date = $request->input('date');
        $schedules = $request->input('schedule');
        $date = explode(" ", $schedules[0]['start_time']);
        $date = $date[0];

        //Fetching Timezone of Admin..
        $fetchAdminTimezone = AdminSchedule::fetchAdminTimezone($adminId);
        $adminTimezone = $fetchAdminTimezone->name;

        $dataArray = [];
        foreach ($schedules as $schedule) {
            $startDate  = explode(" ", $schedule['start_time']);
            $startDate  = $startDate[0];
            $endDate    = explode(" ", $schedule['end_time']);
            $endDate    = $endDate[0];
            $data = [
                'admin_id'      => $adminId,
                'start_date'    => $startDate,
                'end_date'      => $endDate,
                'start_time'    => GoToWebinarController::convertUTC($schedule['start_time'], $adminTimezone),
                'end_time'      => GoToWebinarController::convertUTC($schedule['end_time'], $adminTimezone),
                'created_at'    => now(),
                'updated_at'    => now(),
            ];
            array_push($dataArray , $data);
        }
        
        $startTimes = array_column($dataArray, 'start_time');
        $repatedTimes   = collect();
        for ($i=0; $i < count($startTimes); $i++) {
            for($j=$i; $j < count($startTimes); $j++) {
                if (isset($startTimes[$j+1])) {
                    $checkInterval = Carbon::parse($startTimes[$i])->diffInHours(Carbon::parse($startTimes[$j+1]));
                    if($checkInterval == 0) {
                        $repatedTimes->push(($i+1).' schedule - '.($j+2).' schedule');
                    }
                }
            }
        }
        if ($repatedTimes->isNotEmpty()) {
            return HttpResponse::error($repatedTimes->join(' and ').' are repated', 400);
        }
        //Fetching DB Records of Scheduled Date..
        $dbStartTimes = AdminSchedule::fetchRecordsOfScheduledDate($adminId, $date)->toArray();
        if (isset($dbStartTimes)) {
            $repatedTimes = collect();
            for ($i=0; $i < count($startTimes); $i++) {
                if (in_array($startTimes[$i], $dbStartTimes)) {
                    $repatedTimes->push(($i+1).' schedule');
                }
            }
            if ($repatedTimes->isNotEmpty()) {
                return HttpResponse::error($repatedTimes->join(' and ').' are already generated', 400);
            }
        }
        AdminSchedule::StoreAdminSchedule($dataArray);
        return HttpResponse::success(["data" => 'Schedules created successfully']);

    }

    public function goToWebinarUpdateScheduleByAdmin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'admin_id'  => 'required|exists:users,id',
            'schedule'  => 'required|array',
            'schedule.*.id' => 'nullable|exists:admin_schedules,id',
            'schedule.*.start_time' => 'required|date_format:Y-m-d H:i:s|before:schedule.*.end_time|after_or_equal:'.date('Y-m-d H:i:s'),
            'schedule.*.end_time'   => 'required|date_format:Y-m-d H:i:s|after:schedule.*.start_time',
        ]);
        
        if ($validator->fails()) {
            throw new InvalidRequestException($validator);
        }

        $adminId = $request->input('admin_id');
        //Fetching Timezone of Admin..
        $fetchAdminTimezone = AdminSchedule::fetchAdminTimezone($adminId);
        $adminTimezone = $fetchAdminTimezone->name;

        $date       = $request->input('date');
        $schedules  = $request->input('schedule');
        $date       = explode(" ", $schedules[0]['start_time']);
        $date       = $date[0];

        //Fetching Records of isAlreadyBooked..
        $scheduleIdArray  = array_column($schedules, 'id');
        $adminScheduleIds = AdminSchedule::fetchRecordsOfScheduledIds($adminId, $date)->toArray();
        if (isset($scheduleIdArray) && count($adminScheduleIds)) {
            $deleteScheduleIds = array_diff($adminScheduleIds, $scheduleIdArray);
            if(count($deleteScheduleIds)) {
                $scheduleIds   = AdminSchedule::isBookedAdminSchedule($deleteScheduleIds);
                if(count($scheduleIds)) {
                    return HttpResponse::error("Schedules are already booked", 400);
                }
                AdminSchedule::deleteAdminSchedule($deleteScheduleIds);
            }
        }
        
        $dataArray = [];
        foreach ($schedules as $schedule) {
            $startDate  = explode(" ", $schedule['start_time']);
            $startDate  = $startDate[0];
            $endDate    = explode(" ", $schedule['end_time']);
            $endDate    = $endDate[0];
            $startTime  = explode(" ", $schedule['start_time']);
            $startTime  = $startTime[1];
            $endTime    = explode(" ", $schedule['end_time']);
            $endTime    = $endTime[1];
            $data = [
                'id'            => $schedule['id'],
                'admin_id'      => $adminId,
                'start_date'    => $startDate,
                'end_date'      => $endDate,
                'start_time'    => $schedule['id'] ? $startTime : GoToWebinarController::convertUTC($schedule['start_time'], $adminTimezone),
                'end_time'      => $schedule['id'] ? $endTime : GoToWebinarController::convertUTC($schedule['end_time'], $adminTimezone),
            ];
            array_push($dataArray , $data);
        }
        $startTimes     = array_column($dataArray, 'start_time');
        $repatedTimes   = collect();
        for ($i=0; $i < count($startTimes); $i++) {
            for($j=$i; $j < count($startTimes); $j++) {
                if (isset($startTimes[$j+1])) {
                    $checkInterval = Carbon::parse($startTimes[$i])->diffInHours(Carbon::parse($startTimes[$j+1]));
                    if($checkInterval == 0) {
                        $repatedTimes->push(($i+1).' schedule - '.($j+2).' schedule');
                    }
                }                         
            }
        }
        if ($repatedTimes->isNotEmpty()) {
            return HttpResponse::error($repatedTimes->join(' and ').' are repated', 400);
        }
        AdminSchedule::UpdateAdminSchedule($dataArray);
        return HttpResponse::success(["data" => 'Schedules updated or created successfully']);

    }

    public static function convertUTC($time, $timezone)
    {
        $timeUTC = Carbon::createFromFormat('Y-m-d H:i:s', $time, $timezone);
        $timeUTC->setTimezone('UTC');
        $timeUTC = explode(" ", $timeUTC);
        return $timeUTC[1];
    }

    public function goToWebinarAppointmentBookingByUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'admin_id'      => 'required|exists:users,id',
            'user_id'       => 'required|exists:users,id',
            'schedule_ids'      => 'required|array|exists:admin_schedules,id',
        ]);
        
        if ($validator->fails()) {
            throw new InvalidRequestException($validator);
        }

        $adminId = $request->input('admin_id');
        $userId = $request->input('user_id');
        $scheduleIds = $request->input('schedule_ids');
        
        //Checking Schedules are available or not and date-time more than now..
        $adminSchedules = AdminSchedule::getAdminScheduleByIds($scheduleIds);
        $expiredBookedArray = collect();
        for ($i=0; $i < count($scheduleIds); $i++) {
            $validSchedule = $adminSchedules->where('id', '=', $scheduleIds[$i])->first();
            if ($validSchedule['date'] < now()->toDateString()) {
                $expiredBookedArray->push(($i+1).' schedule expired');
            } else{
                $checkInterval = Carbon::parse($validSchedule['date'].' '.$validSchedule['start_time'])->diffInHours(Carbon::now());
                if ($checkInterval == 0) {
                    $expiredBookedArray->push(($i+1).' schedule expired');
                }
            }
        }
        if ($expiredBookedArray->isNotEmpty()) {
            return HttpResponse::error($expiredBookedArray->join(' and '), 400);
        }
        // Checking Schedules are available or not..
        $bookedArray = collect();
        for($i=0; $i < count($scheduleIds); $i++) {
            $validSchedule = $adminSchedules->where('id', '=', $scheduleIds[$i])->first();
            if($validSchedule['is_booked'] == 'true') {
                $expiredBookedArray->push(($i+1).' schedule already booked');
            } else {
                $times[$i]['startTime'] = $validSchedule['date'].'T'.$validSchedule['start_time'].'Z';
                $times[$i]['endTime'] = $validSchedule['date'].'T'.$validSchedule['end_time'].'Z';
            }
        }
        if ($bookedArray->isNotEmpty()) {
            return HttpResponse::error($bookedArray->join(' and ').' are already booked', 400);
        }
        $timeJSON = json_encode($times);
        //validate access token
        $accessToken = GoToWebinarController::AccessTokenValidator();
        $fetchorganizerKey = GoToWebinar::fetchGoToWebinarAccessDetails('ORGANIZER_KEY');
        $organizerKey = $fetchorganizerKey['value'];
        if (is_array($accessToken)) {
            return HttpResponse::error($accessToken, 400);
        } elseif (is_string($accessToken)) {
            $body = '{
                "subject": "xyz",
                "description": "xyz",
                "times": '.$timeJSON.',
                "timeZone": "UTC",
                "type": "series",
                "isPasswordProtected": false,
                "recordingAssetKey": "yes",
                "isOndemand": false,
                "experienceType": "CLASSIC",
                "emailSettings": {
                  "confirmationEmail": {
                    "enabled": true
                  },
                  "reminderEmail": {
                    "enabled": true
                  },
                  "absenteeFollowUpEmail": {
                    "enabled": true
                  },
                  "attendeeFollowUpEmail": {
                    "enabled": true,
                    "includeCertificate": true
                  }
                }
            }';
            $headers = [
                'Authorization' => 'Bearer '.$accessToken,
                'Accept'        => 'application/json',
                'Content-Type'  => 'text/plain',
            ];
            $url = env('GOTOWEBINAR_BASEURL').$organizerKey.'/webinars';
            $getWebinarKey = HttpRequest::clientPost($url, $headers, $body);
            $webinarKey = $getWebinarKey['webinarKey'];
            GoToWebinar::storeWebinarKey($webinarKey);
            if (!isset($getWebinarKey['error'])) {
                $webinarKey = $getWebinarKey['webinarKey'];
                GoToWebinar::storeWebinarKey($webinarKey);
                $body = '[{
                    "external": true,
                    "organizerKey": "'.$organizerKey.'",
                    "givenName": "abc",
                    "email": "abc@fhg.com"
                }]';
                $url = env('GOTOWEBINAR_BASEURL').$organizerKey.'/webinars'.'/'.$webinarKey.'/coorganizers';
                $getCoOrganizer = HttpRequest::clientPost($url, $headers, $body);
                if (!isset($getCoOrganizer['error'])) {
                    $adminMemberKey = $getCoOrganizer[0]['memberKey'];
                    $adminJoinLink  = $getCoOrganizer[0]['joinLink'];
                    $randomNameEmail = substr(str_shuffle('abcdefghijklmnopqrstuvwxyz'),1,5);
                    $body = '{
                        "firstName": "'.$randomNameEmail.'",
                        "lastName": "a",
                        "email": "'.$randomNameEmail.'@qwre.com",
                    }';
                    $url = env('GOTOWEBINAR_BASEURL').$organizerKey.'/webinars'.'/'.$webinarKey.'/registrants';
                    $getRegistrants = HttpRequest::clientPost($url, $headers, $body);
                    if (!isset($getRegistrants['error'])) {
                        $userRegistrantKey = $getRegistrants['registrantKey'];
                        $userJoinLink  = $getRegistrants['joinUrl'];
                        $amount =  ((env('GOTOWEBINAR_COST_FOR_PERHOUR')) * (count($scheduleIds)));
                        //Storing into Appointment Table
                        $data = [
                            'admin_id'   => $adminId,
                            'user_id'    => $userId,
                            'amount'     => $amount,
                            'webinar_key'=> $webinarKey,
                            'status'     => 'booked',
                            'created_at' => now(),
                            'updated_at' => now()
                        ];
                        $AppointmentId = Appointment::StoreAppointment($data);
                        //Storing into Appointment Schedule Xrefs Table
                        $dataArray = [];
                        foreach ($scheduleIds as $schedule) {
                            $data = [
                                'appointment_id'=> $AppointmentId,
                                'schedule_id'   => $schedule,
                                'user_id'       => $userId,
                                'admin_member_key' => $adminMemberKey,
                                'admin_join_link' => $adminJoinLink,
                                'user_registrant_key' => $userRegistrantKey,
                                'user_join_link' => $userJoinLink,
                                'created_at'    => now(),
                                'updated_at'    => now()
                            ];
                            array_push($dataArray , $data);
                        }
                        AppointmentSchedulerXref::StoreBulkAppointmentSchedule($dataArray);
                        AdminSchedule::UpdateIsBookedAdminSchedule($scheduleIds);
                        return HttpResponse::success(["data" => 'Appointment has Scheduled Successfully']);
                    } else {
                        return HttpResponse::error($getRegistrants['error'], 400);
                    }
                } else {
                    return HttpResponse::error($getCoOrganizer['error'], 400);
                }
            } else {
                return HttpResponse::error($getWebinarKey['error'], 400);
            }
        } else {
            return HttpResponse::error('No Data', 400);
        }
    }
}