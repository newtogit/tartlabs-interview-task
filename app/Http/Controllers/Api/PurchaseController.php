<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use App\Models\Purchase;
use App\Models\User;
use App\Models\Role;
use App\Library\HttpResponse;
use App\Http\Resources\Purchase as PurchaseResource;
use SebastianBergmann\CodeCoverage\Driver\Selector;

class PurchaseController extends Controller
{
    protected $with = ['purchaseable', 'user'];
    
    public function index()
    {
        $userId = auth()->user()->id;
        $roleId = Role::getRoleId($userId);
        //if user has not assigned any role it return 0
        if ($roleId == '0') {
            return HttpResponse::error('No Data', 400);
        }
        else if ($roleId == '1') {
            $purchases = Purchase::getPurchase($this->with);
        } else {
            $purchases = Purchase::getPurchaseByUser($userId, $this->with);
        }
        if ($purchases) {
            $data = new PurchaseResource($purchases);
            return HttpResponse::success(["data" => $data]);
        } else {
            return HttpResponse::error('No Data', 400);
        }
    }
}