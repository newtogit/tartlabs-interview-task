<?php

namespace App\Http\Controllers;

use App\Library\FileUpload;
use App\Library\RazorpayPayment;
use App\Models\Service;
use App\Models\Payment;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:service-list|service-create|service-edit|service-delete', ['only' => ['index','show']]);
        $this->middleware('permission:service-create', ['only' => ['create','store']]);
        $this->middleware('permission:service-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:service-delete', ['only' => ['destroy']]);
        $this->middleware('auth');
    }

    public function index()
    {
        $services = Service::getServiceDetails();
        return view('services.index',compact('services'))
            ->with('i', (request()->input('page', 1) - 1) * 10);
    }

    public function create()
    {
        return view('services.create');
    }

    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'detail' => 'required',
            'file' => 'image|mimes:jpg,jpeg,png,gif,svg|max:2048'
        ]);
        $image = $request->file('file');
        $fileName = '';
        if ($image) {
            $fileName = FileUpload::store('services', $image);
        }
        $serviceData = [
            'name' => $request->input('name'),
            'detail' => $request->input('detail'),
            'image' => $fileName
        ];
        Service::storeServiceDetails($serviceData);
        return redirect()->route('services.index')->with('success','Service created successfully.');
    }

    public function show(Service $service)
    {
        return view('services.show',compact('service'));
    }

    public function edit(Service $service)
    {
        return view('services.edit',compact('service'));
    }

    public function update(Request $request, Service $service)
    {
        request()->validate([
            'name' => 'required',
            'detail' => 'required',
        ]);
        $id = $service->id;
        $image = $request->file('file');
        $fileName = $request->input('oldFile');
        if ($image) {
            $OldFileName = $request->input('oldFile');
            $fileName = FileUpload::store('services', $image, $OldFileName);
        }
        $serviceData = [
            'name' => $request->input('name'),
            'detail' => $request->input('detail'),
            'image' => $fileName
        ];
        Service::updateServiceDetails($id, $serviceData);
        return redirect()->route('services.index')->with('success','Service updated successfully.');
    }

    public function destroy(Service $service)
    {
        Service::destoryServiceDetails($service->id);
        return redirect()->route('services.index')->with('success','Service deleted successfully.');
    }

    public function purchase(Request $request, $id)
    {
        $userId = auth()->user()->id;
        $input = $request->all();
        $payment = RazorpayPayment::store($input);
        $payInfo = [
            'productOrServiceId' => $id,
            'userId' => $userId,
            'razorpayPaymentId' => $payment['razorpay_payment_id'],
            'amount' => $payment['amount'],
            'type' => 'service',
        ];
        Payment::storePurchase($payInfo);
        return redirect()->route('services.index')->with('success','Service purchased successfully.');
    }
}