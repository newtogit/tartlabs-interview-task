<?php

namespace App\Http\Controllers;

use App\Library\FileUpload;
use App\Library\RazorpayPayment;
use App\Models\Product;
use App\Models\Payment;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:product-list|product-create|product-edit|product-delete', ['only' => ['index','show']]);
        $this->middleware('permission:product-create', ['only' => ['create','store']]);
        $this->middleware('permission:product-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:product-delete', ['only' => ['destroy']]);
        $this->middleware('auth');
    }

    public function index()
    {
        $products = Product::getProductDetails();
        return view('products.index',compact('products'))
            ->with('i', (request()->input('page', 1) - 1) * 10);
    }

    public function create()
    {
        return view('products.create');
    }

    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'detail' => 'required',
            'file' => 'image|mimes:jpg,jpeg,png,gif,svg|max:2048'
        ]);
        $image = $request->file('file');
        $fileName = '';
        if ($image) {
            $fileName = FileUpload::store('products', $image);
        }
        $productData = [
            'name' => $request->input('name'),
            'detail' => $request->input('detail'),
            'image' => $fileName
        ];
        Product::storeProductDetails($productData);
        return redirect()->route('products.index')->with('success','Product created successfully.');
    }

    public function show(Product $product)
    {
        return view('products.show',compact('product'));
    }

    public function edit(Product $product)
    {
        return view('products.edit',compact('product'));
    }

    public function update(Request $request, Product $product)
    {
        request()->validate([
            'name' => 'required',
            'detail' => 'required',
        ]);
        $id = $product->id;
        $image = $request->file('file');
        $fileName = $request->input('oldFile');
        if ($image) {
            $OldFileName = $request->input('oldFile');
            $fileName = FileUpload::store('products', $image, $OldFileName);
        }
        $productData = [
            'name' => $request->input('name'),
            'detail' => $request->input('detail'),
            'image' => $fileName
        ];
        Product::updateProductDetails($id, $productData);
        return redirect()->route('products.index')->with('success','Product updated successfully.');
    }

    public function destroy(Product $product)
    {
        Product::destoryProductDetails($product->id);
        return redirect()->route('products.index')->with('success','Product deleted successfully.');
    }

    public function purchase(Request $request, $id)
    {
        $userId = auth()->user()->id;
        $input = $request->all();
        $payment = RazorpayPayment::store($input);
        $payInfo = [
            'productOrServiceId' => $id,
            'userId' => $userId,
            'razorpayPaymentId' => $payment['razorpay_payment_id'],
            'amount' => $payment['amount'],
            'type' => 'product',
        ];
        Payment::storePurchase($payInfo);
        return redirect()->route('products.index')->with('success','Product purchased successfully.');
    }
}