<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Role;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $userId = auth()->user()->id;
        $roleId = Role::getRoleId($userId);
        if ($roleId == '1') {
            return redirect('users');
        }
        return view('home');
    }
}