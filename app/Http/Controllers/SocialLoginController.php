<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Exception;

class SocialLoginController extends Controller
{
    use AuthenticatesUsers;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }
    
    public function handleProviderCallback($provider)
    {
        try {
            $user = Socialite::driver($provider)->user();
            $finduser = User::verifyUser($user->email);
            if ($finduser) {
                SocialLoginController::authLogin($finduser);
                return redirect('/');
            } else {
                $newUser = [
                    'name' => $user->name,
                    'email' => $user->email,
                    'provider_name' => $provider,
                    'provider_id' => $user->id,
                    'password' => Hash::make('12345')
                ];
                $user = User::storeUser($newUser);
                SocialLoginController::authLogin($user);
                return redirect('/');
            }
        } catch (Exception $exception) {
            dd($exception->getMessage());
        }
    }

    public static function authLogin($finduser)
    {
        Auth::login($finduser);
    }

}