<?php
namespace App\Http\Middleware;
use App\Exceptions\AppException;
use App\Exceptions\InvalidRequestException;
use App\Library\HttpResponse;
use Closure;
class ExceptionHandlerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $response = $next($request);
            // Was an exception thrown? If so and available catch in our middleware
            if (isset($response->exception) && $response->exception) {
                throw $response->exception;
            }
            return $response;
        } catch (InvalidRequestException $e) {
            $errorResponse = [
                'error' => $this->isJson($e->getMessage()) ? json_decode($e->getMessage()) : $e->getMessage()
            ];
            return HttpResponse::error($errorResponse, ($e->getCode() != null && $e->getCode() != 0) ? $e->getCode() : 400);
        } catch (AppException $e) {
            if ($this->isJson($e->getMessage())) {
                return HttpResponse::error(json_decode($e->getMessage()), ($e->getCode() != null && $e->getCode() != 0) ? $e->getCode() : 400);
            }
            return HttpResponse::error($e->getMessage(), ($e->getCode() != null && $e->getCode() != 0) ? $e->getCode() : 400);
        }
    }
    public function isJson($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}