<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;

    protected $guarded = [];

    public static function addCity($state_id, $city_name)
    {
        $city = City::updateOrCreate([
            'name' => $city_name, 'state_id' => $state_id
        ]);
        return $city->id;
    }
}