<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GoToWebinar extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'go_to_webinar_settings';

    public static function storeAccessTokenOrganizationKey($data)
    {
        return GoToWebinar::insert($data);
    }

    public static function accessTokenUpdate($key, $data)
    {
        return GoToWebinar::where('key', $key)->update($data);
    }

    public static function fetchGoToWebinarAccessDetails($data)
    {
        return GoToWebinar::where('key', '=', $data)->first();
    }

    public static function storeWebinarKey($data)
    {
        return GoToWebinar::updateOrCreate(
            ['key' => 'WEBINAR_KEY'],
            ['value' => $data]
        );
    }
    
}