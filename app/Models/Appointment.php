<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Appointment extends Model
{
    use HasFactory;

    protected $guarded = [];

    public static function StoreAppointment($appointmentData)
    {
        return $id = Appointment::insertGetId($appointmentData);
    }

}