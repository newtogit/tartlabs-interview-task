<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AdminSchedule extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'admin_schedules';

    public function appointments()
    {
        return $this->belongsToMany(Appointment::class);
    }

    public static function fetchAdminTimezone($id)
    {
        return DB::table('timezones')
            ->select('timezones.name')
            ->join('users', 'users.timezone_id', '=', 'timezones.id')
            ->where('users.id', '=', $id)
            ->first();
    }

    public static function StoreAdminSchedule($scheduleData)
    {
        AdminSchedule::insert($scheduleData);
    }

    public static function UpdateAdminSchedule($scheduleData)
    {
        foreach ($scheduleData as $data){
            AdminSchedule::updateOrCreate(
                ['id' => $data['id'], 'start_date' => $data['start_date']],
                $data
            );
        }
    }
    
    public static function fetchRecordsOfScheduledDate($id, $date)
    {
        return AdminSchedule::where('admin_id', $id)->where('start_date', $date)->pluck('start_time');
        
    }
    
    public static function fetchRecordsOfScheduledIds($id, $date)
    {
        return AdminSchedule::where('admin_id', $id)->where('start_date', $date)->pluck('id');
        
    }

    public static function isBookedAdminSchedule($ids)
    {
        return AdminSchedule::whereIn('id', $ids)->where('is_booked', 'true')->get();
    }

    public static function getAdminScheduleByIds($ids)
    {
        return AdminSchedule::whereIn('id', $ids)->get();
    }

    public static function UpdateIsBookedAdminSchedule($ids)
    {
        AdminSchedule::whereIn('id', $ids)->update(['is_booked' => 'true']);
    }
    public static function deleteAdminSchedule($ids)
    {
        AdminSchedule::whereIn('id', $ids)->delete();
    }
}