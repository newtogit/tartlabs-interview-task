<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use App\Library\HttpResponse;
use App\Library\FileUpload;

class Service extends Model
{
    use HasFactory;

    protected $guarded = [];
    
    public function purchases(){
        return $this->MorphMany(Purchase::class, 'purchaseable');
    }

    public static function getServiceDetails()
    {
        return Service::latest()->simplePaginate(10);
    }

    public static function storeServiceDetails($serviceData)
    {
        Service::create($serviceData);
    }

    public static function updateServiceDetails($id, $serviceData)
    {
        Service::where('id', $id)->update($serviceData);
    }
    
    public static function destoryServiceDetails($id)
    {
        $services = Service::find($id);
        FileUpload::destory('services', $services->image);
        $services->delete();
    }
}