<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Event;
use App\Events\SendMail;

class Payment extends Model
{
    use HasFactory;

    protected $guarded = [];

    public static function storePurchase($payInfo)
    {
        if($payInfo['type'] == 'product') {
            $purchase = Product::find($payInfo['productOrServiceId']);
        } else {
            $purchase = Service::find($payInfo['productOrServiceId']);
        }
        $purchaseId = $purchase->purchases()->create([
            'user_id' => $payInfo['userId'],
            'status' => 'success',
        ]);
        //Payment Table
        $amount = ($payInfo['amount']/100);
        $payInfoStore = [
            'payment_id' => $payInfo['razorpayPaymentId'],
            'user_id' => $payInfo['userId'],
            'purchase_id' => $purchaseId->id,
            'amount' => $amount,
        ];
        Payment::create($payInfoStore);
        //Sending Email
        Event::dispatch(new SendMail($payInfo['userId'], $purchase));
    }

}
