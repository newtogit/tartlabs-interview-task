<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use App\Library\HttpResponse;
use App\Library\FileUpload;

class Product extends Model
{
    use HasFactory;

    protected $guarded = [];
    
    public function purchases(){
        return $this->MorphMany(Purchase::class, 'purchaseable');
    }

    public static function getProductDetails()
    {
        return Product::latest()->simplePaginate(10);
    }

    public static function storeProductDetails($productData)
    {
        Product::create($productData);
    }

    public static function updateProductDetails($id, $productData)
    {
        Product::where('id', $id)->update($productData);
    }
    
    public static function destoryProductDetails($id)
    {
        $products = Product::find($id);
        FileUpload::destory('products', $products->image);
        $products->delete();
    }
}