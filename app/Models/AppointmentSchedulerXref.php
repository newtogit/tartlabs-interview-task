<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AppointmentSchedulerXref extends Model
{
    use HasFactory;

    protected $guarded = [];  

    public static function StoreBulkAppointmentSchedule($appointmentData)
    {
        AppointmentSchedulerXref::insert($appointmentData);
    }
}