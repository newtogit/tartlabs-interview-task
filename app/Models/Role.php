<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Role extends Model
{
    use HasFactory;

    protected $guarded = [];

    public static function getRoleId($userId = null)
    {
        return DB::table('model_has_roles')->where('model_id', $userId)->first()->role_id ?? 0;
    }

}
