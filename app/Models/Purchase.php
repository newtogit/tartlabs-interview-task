<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    use HasFactory;

    protected $guarded = [];
    
    public function purchaseable()
    {
        return $this->morphTo();
    }

    public static function getPurchase($with = [])
    {
        return Purchase::with($with)->get();
    }

    public static function getPurchaseByUser($userId, $with = [])
    {
        return Purchase::where('user_id', '=', $userId)->with($with)->get();
    }
    
    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}