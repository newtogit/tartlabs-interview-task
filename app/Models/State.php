<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    use HasFactory;

    protected $guarded = [];

    public static function addState($country_id, $state_name)
    {
        $state = State::updateOrCreate([
            'name' => $state_name, 'country_id' => $country_id
        ]);
        return $state->id;
    }
}