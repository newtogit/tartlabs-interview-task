<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentSchedulerXrefsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointment_scheduler_xrefs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('appointment_id')->constrained('appointments');
            $table->foreignId('schedule_id')->constrained('admin_schedules');
            $table->foreignId('user_id')->constrained('users');
            $table->string('admin_member_key')->nullable();
            $table->string('admin_join_link')->nullable();
            $table->string('user_registrant_key')->nullable();
            $table->string('user_join_link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointment_scheduler_xrefs');
    }
}
