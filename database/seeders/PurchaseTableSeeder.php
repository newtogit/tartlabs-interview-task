<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Product;
use App\Models\Service;
use App\Models\Purchase;

class PurchaseTableSeeder extends Seeder
{

    public function run()
    {
        // $faker = \Faker\Factory::create();

        // foreach(range(1,10) as $value){
        //     Purchase::create([
        //         "user_id" => User::inRandomOrder(2,10)->first()->id,
        //         'purchaseable_id' => rand(1, 20),
        //         'purchaseable_type' => rand(1, 20) == 1 ? 'App\Models\Product' : 'App\Models\Service',
        //         // "product_id" => Product::inRandomOrder()->first()->id,
        //         // "service_id" => Service::inRandomOrder()->first()->id,
        //         "status" => 'success',
        //     ]);
        // }

        Purchase::create([
            "user_id" => '2',
            'purchaseable_id' => '1',
            'purchaseable_type' => 'App\Models\Product',
            "status" => 'success',
        ]);

        Purchase::create([
            "user_id" => '2',
            'purchaseable_id' => '1',
            'purchaseable_type' => 'App\Models\Service',
            "status" => 'success',
        ]);

    }
}