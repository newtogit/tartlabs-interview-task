<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ServiceController;


use App\Http\Controllers\SocialLoginController;

Auth::routes();

Route::get('/', [HomeController::class, 'index']);
Route::get('/home', [HomeController::class, 'index']);

Route::post('/products/{id}/purchase', [ProductController::class, 'purchase'])->name('products.purchase');
Route::post('/services/{id}/purchase', [ServiceController::class, 'purchase'])->name('services.purchase');

Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    Route::resource('products', ProductController::class);
    Route::resource('services', ServiceController::class);
});

Route::get('auth/{provider}', [SocialLoginController::class, 'redirectToProvider'])
    ->name('auth')
    ->where('provider', implode('|', config('auth.socialite.drivers')));
Route::get('auth/{provider}/callback', [SocialLoginController::class, 'handleProviderCallback']);