<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\RegisterController;
use App\Http\Controllers\API\PurchaseController;
use App\Http\Controllers\API\SocialLoginController;
use App\Http\Controllers\API\UserProfileUpdateController;
use App\Http\Controllers\API\GoToWebinarController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('register', [RegisterController::class, 'register']);
Route::post('login', [RegisterController::class, 'login']);

Route::middleware('auth:api')->group( function () {
    Route::resource('purchase', PurchaseController::class);
    Route::post('user-profile-update', [UserProfileUpdateController::class, 'userProfileUpdate']);
    Route::post('user-reports', [UserProfileUpdateController::class, 'getUserReports']);
});

Route::post('auth/login', [SocialLoginController::class, 'handleProviderCallback']);

Route::post('fb/login', [SocialLoginController::class, 'fbLogin']);
Route::post('linkedin/login', [SocialLoginController::class, 'linkedinLogin']);

Route::post('auth/gotowebinar', [GoToWebinarController::class, 'goToWebinar']);
Route::get('auth/gotowebinarprofile', [GoToWebinarController::class, 'goToWebinarProfile']);
Route::get('auth/gotowebinarcreatewebinar', [GoToWebinarController::class, 'goToWebinarCreateWebinar']);

Route::post('auth/gotowebinarcreateschedulebyadmin', [GoToWebinarController::class, 'goToWebinarCreateScheduleByAdmin']);
Route::post('auth/gotowebinarupdateschedulebyadmin', [GoToWebinarController::class, 'goToWebinarUpdateScheduleByAdmin']);
Route::post('auth/gotowebinarappointmentbookingbyuser', [GoToWebinarController::class, 'goToWebinarAppointmentBookingByUser']);
